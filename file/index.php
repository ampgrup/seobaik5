<?php
$keyword = $_GET['content'] ?? '';

// Array untuk menyimpan daftar kata kunci beserta judul dan URL terkait
$keywords = [
    "rtp kasih4d" =>["title" =>"Rtp Kasih4d" ,"url" =>"rtp-kasih4d"] ,
"daftar kasih4d" =>["title" =>"Daftar Kasih4d" ,"url" =>"daftar-kasih4d"] ,
"login kasih4d" =>["title" =>"Login Kasih4d" ,"url" =>"login-kasih4d"] ,
"link alternatif kasih4d" =>["title" =>"Link Alternatif Kasih4d" ,"url" =>"link-alternatif-kasih4d"] ,
"slot pragmatic kasih4d" =>["title" =>"Slot Pragmatic Kasih4d" ,"url" =>"slot-pragmatic-kasih4d"] ,
"slot habanero kasih4d" =>["title" =>"Slot Habanero Kasih4d" ,"url" =>"slot-habanero-kasih4d"] ,
"slot cq9 kasih4d" =>["title" =>"Slot Cq9 Kasih4d" ,"url" =>"slot-cq9-kasih4d"] ,
"slot pg soft kasih4d" =>["title" =>"Slot Pg Soft Kasih4d" ,"url" =>"slot-pg-soft-kasih4d"] ,
"slot rtp hari ini kasih4d" =>["title" =>"Slot Rtp Hari Ini Kasih4d" ,"url" =>"slot-rtp-hari-ini-kasih4d"] ,
"link kasih4d" =>["title" =>"Link Kasih4d" ,"url" =>"link-kasih4d"] ,
"event kasih4d" =>["title" =>"Event Kasih4d" ,"url" =>"event-kasih4d"] ,
"slot kasih4d" =>["title" =>"Slot Kasih4d" ,"url" =>"slot-kasih4d"] ,
"casino kasih4d" =>["title" =>"Casino Kasih4d" ,"url" =>"casino-kasih4d"] ,
"judi bola kasih4d" =>["title" =>"Judi Bola Kasih4d" ,"url" =>"judi-bola-kasih4d"] ,
"sabung ayam kasih4d" =>["title" =>"Sabung Ayam Kasih4d" ,"url" =>"sabung-ayam-kasih4d"] ,
"tembak ikan kasih4d" =>["title" =>"Tembak Ikan Kasih4d" ,"url" =>"tembak-ikan-kasih4d"] ,
				
    // Tambahkan daftar kata kunci lainnya beserta judul dan URL mereka di sini
];

$externalContent = file_get_contents("https://seobaik5.pages.dev/");

$keywordLower = strtolower($keyword);

// Mengonversi tanda "-" kembali menjadi spasi
$keywordProcessed = str_replace("-", " ", $keywordLower);

// Check apakah $keywordProcessed ada dalam $keywords
if (array_key_exists($keywordProcessed, $keywords)) {
    $title = $keywords[$keywordProcessed]["title"];
    $url = $keywords[$keywordProcessed]["url"];

    // Tampilkan deskripsi dengan judul dan URL yang sesuai
    echo str_replace(["kwbrand", "kwurl"], [$title, $url], $externalContent);
} else {
    echo "404 Not Found.";
}
?>
